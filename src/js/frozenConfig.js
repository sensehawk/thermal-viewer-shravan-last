export const ConfigMap = {
  table: {
    class_id: 100,
    description: "Table"
  },
  hotspot: {
    class_id: 101,
    description: "Hotspot"
  },
  module_failure: {
    class_id: 103,
    description: "Module Failure"
  },
  string_failure: {
    class_id: 105,
    description: "String Failure"
  },
  diode_failure: {
    class_id: 102,
    description: "Diode Failure"
  },
  string_reverse_polarity: {
    class_id: 106,
    description: "String Reverse Polarity"
  },
  module_reverse_polarity: {
    class_id: 104,
    description: "Vegetation"
  }
};

export const ConfigArray = [
  {
    class_id: 100,
    class_name: "table",
    description: "Table",
    color: "rgb(44, 62, 80)",
    group: "default",
    num_shortcut: 1
  },
  {
    class_id: 101,
    class_name: "hotspot",
    description: "Hotspot",
    color: "rgb(0, 28, 99)",
    group: "default",
    num_shortcut: 1
  },
  {
    class_id: 102,
    class_name: "diode_failure",
    description: "Diode Failure",
    color: "rgb(66, 238, 244)",
    group: "default",
    num_shortcut: 4
  },
  {
    class_id: 103,
    class_name: "module_failure",
    description: "Module Failure",
    color: "rgb(46, 204, 113)",
    group: "default",
    num_shortcut: 2
  },
  {
    class_id: 104,
    class_name: "module_reverse_polarity",
    description: "Vegetation",
    color: "rgb(255, 132, 220)",
    group: "default",
    num_shortcut: 5
  },
  {
    class_id: 105,
    class_name: "string_failure",
    description: "String Failure",
    color: "rgb(46, 204, 113)",
    group: "default",
    num_shortcut: 3
  },
  {
    class_id: 106,
    class_name: "string_reverse_polarity",
    description: "String Reverse Polarity",
    color: "rgb(255, 132, 220)",
    group: "default",
    num_shortcut: 5
  }
];
