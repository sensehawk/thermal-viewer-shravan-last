import { ConfigMap } from "../js/frozenConfig";
import { INITIAL_URL } from "../constants";

export default {
  methods: {
    loadStandaloneFileWithID() {
      const initialJSONUrl = INITIAL_URL + this.$store.getters.projectId + "/initial.json"

      // http://13.233.198.28:8080/thermal_parsed.json/
      console.log(`Fetching initial JSON from ${initialJSONUrl}`);
      fetch(initialJSONUrl)
        .then(res => res.json())
        .then(jsonData => {
          const groupedData = {};

          for (let proj of jsonData) {
            if (groupedData[proj.groupName]) {
              groupedData[proj.groupName].push(proj);
            } else {
              groupedData[proj.groupName] = [proj];
            }
          }
          this.$store.dispatch("SET_ALL_FEATURE_TYPES", Object.keys(ConfigMap));
          this.$store.dispatch("SET_MAP_DATA", groupedData);
        });
    }
  }
};
