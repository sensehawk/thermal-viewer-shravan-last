For coloring the vectors drawn with gl-draw
---------------------------------------------------
const stops = [[1, "green"], [2, "violet"], [3, "red"]];
map.setPaintProperty("gl-draw-polygon-stroke-inactive.cold", "line-color", {"property": "user_class_id", "type": "categorical", "stops": stops, "default": "yellow"});
map.setPaintProperty("gl-draw-polygon-stroke-inactive.hot", "line-color", {"property": "user_class_id", "type": "categorical", "stops": stops, "default": "yellow"});

map.on("draw.create", function(featuresObject){
  const features = featuresObject["features"];
  for (let feature of features){
    draw.setFeatureProperty(feature["id"], "class_id", 2);
  }
})

For loading svgs properly
-------------------------
module.exports = {
  chainWebpack: config => {
    config.module.rules.delete("svg");
  },

  configureWebpack: {
    module: {
      rules: [
        {
          test: /\.svg$/, 
          loader: 'vue-svg-loader', 
        },
      ],
    }      
  },
};

For adding maptile sources and creating layers from them
--------------------------------------------------------
const s = map.addSource('source1', {
  "type": "raster",
  "tiles": ['https://maps.sensehawk.com/b1a84f5f-1a3b-4797-bcd2-ac91022df9a3/{z}/{x}/{y}.png'],
  "tileSize": 256,
});

map.addLayer({
  "id": "own id",
  "source": "source1",
  "type": "raster",
}, "gl-draw-polygon-fill-inactive.cold");

map.addSource('source2', {
  "type": "raster",
  "tiles": ['https://maps.sensehawk.com/e5f608cb-3859-4dc9-a96d-af13774edb2a/{z}/{x}/{y}.png'],
  "tileSize": 256,
});

map.addLayer({
  "id": "own id1",
  "source": "source2",
  "type": "raster",
}, "gl-draw-polygon-fill-inactive.cold");

For importing geoJSON data into GL Draw
---------------------------------------
draw.set(getJSON);