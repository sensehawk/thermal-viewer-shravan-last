// the mapping of Workspace codes and names

export const getDataFromId = function(id) {
  for (let x of configMap) {
    if (x.code === id) {
      return x.fields;
    }
  }
  return null;
}

export const configMap = [
  {
    code: 1,
    fields: {
      name: "Vegetation",
      label: "module_reverse_polarities"
    }
  },
  {
    code: 2,
    fields: {
      name: "String Reverse Polarity",
      label: "string_reverse_polarities"
    }
  },
  {
    code: 3,
    fields: {
      name: "Diode Failures",
      label: "diode_failures"
    }
  },
  {
    code: 4,
    fields: {
      name: "String Failure",
      label: "string_failures"
    }
  },
  {
    code: 5,
    fields: {
      name: "Module Failure",
      label: "module_failures"
    }
  },
  {
    code: 6,
    fields: {
      name: "Hotspot",
      label: "hotspots"
    }
  }
];
