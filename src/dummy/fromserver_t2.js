export default [
  {
    name: "SBEnergy Bhadla Plot L5 Block5 19 July",
    date: "19 July 2018",
    url: "https://maps.sensehawk.com/3a954d99-e666-4a6b-ae89-ca6cae78f05a",
    properties: {
      visible: false
    },
    vectors: {
      type: "FeatureCollection",
      features: []
    }
  },
  {
    name: "SBEnergy Bhadla Plot L5 Block6 19 July",
    date: "19 July 2018",
    url: "https://maps.sensehawk.com/002bac7e-f617-488b-a238-ef1ebc099179",
    properties: {
      opacity: 90,
      visible: false
    },
    vectors: {
      type: "FeatureCollection",
      features: []
    }
  }
];
