export default [
  {
    class_id: 203,
    class_name: "no_rails",
    description: "No Rails",
    color: "red",
    group: "a",
    num_shortcut: 1
  },
  {
    class_id: 204,
    class_name: "partial_rails",
    description: "Partial Rails",
    color: "blue",
    group: "default",
    num_shortcut: 2
  },
  {
    class_id: 205,
    class_name: "full_rails",
    description: "Full Rails",
    color: "green",
    group: "a",
    num_shortcut: 3
  },
  {
    class_id: 206,
    class_name: "partial_modules",
    description: "Partial Modules",
    color: "purple",
    group: "default",
    num_shortcut: 4
  },
  {
    class_id: 207,
    class_name: "full_modules",
    description: "Full Modules",
    color: "white",
    group: "b",
    num_shortcut: 5
  },
  {
    class_id: 208,
    class_name: "full_modulesa",
    description: "Full Modulesa",
    color: "brown",
    group: "b",
    num_shortcut: 5
  },
  {
    class_id: 209,
    class_name: "full_modulesb",
    description: "Full Modulesb",
    color: "violet",
    group: "b",
    num_shortcut: 5
  },
  {
    class_id: 210,
    class_name: "full_modulesc",
    description: "Full Modulesc",
    color: "blue",
    group: "default",
    num_shortcut: 5
  },
  {
    class_id: 2111,
    class_name: "full_modulesd",
    description: "Full Modulesd",
    color: "pink",
    group: "c",
    num_shortcut: 5
  }
];
