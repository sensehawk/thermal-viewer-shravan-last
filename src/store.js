import Vue from "vue";
import Vuex from "vuex";
import { eventBus } from "./eventBus";

Vue.use(Vuex);

const state = {
  projectId: '',
  map: null,
  draw: null,
  isCopyCombinationPressed: false,
  mapData: null,
  configuration: null,
  colorStops: null,
  featureTypeCounts: {},
  allFeatureTypes: [],
  visibleFeatureTypes: [],
  activeFeatureType: null,
  visibleRaster: null,
  lastActiveDrawMode: null,
  isContinuousModeActive: false,
  minTemp: 0,
  maxTemp: 0,
  tempSliderValue: [0, 0]
};

const getters = {
  projectId(state) {
    return state.projectId;
  },
  map(state) {
    return state.map;
  },
  draw(state) {
    return state.draw;
  },
  isCopyCombinationPressed(state) {
    return state.isCopyCombinationPressed;
  },
  mapData(state) {
    return state.mapData;
  },
  configuration(state) {
    return state.configuration;
  },
  colorStops(state) {
    return state.colorStops;
  },
  featureTypeCounts(state) {
    return state.featureTypeCounts;
  },
  allFeatureTypes(state) {
    return state.allFeatureTypes;
  },
  visibleFeatureTypes(state) {
    return state.visibleFeatureTypes;
  },
  activeFeatureType(state) {
    return state.activeFeatureType;
  },
  visibleRaster(state) {
    return state.visibleRaster;
  },
  minTemp(state) {
    return state.minTemp;
  },
  maxTemp(state) {
    return state.maxTemp;
  },
  tempSliderValue(state) {
    return state.tempSliderValue;
  }
};

const mutations = {
  SET_PROJECT_ID(state, payload) {
    state.projectId = payload;
  },
  SET_MAP(state, mapObj) {
    state.map = mapObj;
  },
  SET_DRAW(state, drawObj) {
    state.draw = drawObj;
  },
  SET_COPY_COMBINATION_PRESSED(state, bool) {
    state.isCopyCombinationPressed = bool;
  },
  SET_MAP_DATA(state, mapDataArr) {
    state.mapData = mapDataArr;
  },
  SET_CONFIGURATION(state, configArr) {
    state.configuration = configArr;
  },
  SET_COLORSTOPS(state, payload) {
    state.colorStops = payload;
  },
  SET_FEATURE_TYPE_COUNTS(state, payload) {
    state.featureTypeCounts = payload;
  },
  SET_ALL_FEATURE_TYPES(state, payload) {
    state.allFeatureTypes = payload;
  },
  SET_VISIBLE_FEATURE_TYPES(state, payload) {
    state.visibleFeatureTypes = payload;
  },
  SET_ACTIVE_FEATURE_TYPE(state, payload) {
    state.activeFeatureType = payload;
  },
  SET_MIN_TEMP(state, payload) {
    state.minTemp = payload;
  },
  SET_MAX_TEMP(state, payload) {
    state.maxTemp = payload;
  },
  SET_TEMP_SLIDER_VALUE(state, payload) {
    state.tempSliderValue = payload;
  },
  SET_VISIBLE_RASTER(state, payload) {
    state.visibleRaster = payload;
  }
};

const actions = {
  SET_MAP({ commit }, payload) {
    commit("SET_MAP", payload);
  },
  SET_DRAW({ commit }, payload) {
    commit("SET_DRAW", payload);
  },
  SET_COPY_COMBINATION_PRESSED({ commit }, payload) {
    commit("SET_COPY_COMBINATION_PRESSED", payload);
  },
  SET_MAP_DATA({ commit }, payload) {
    eventBus.$emit("SHOWHIDEBLOCKERMODAL", false);
    commit("SET_MAP_DATA", payload);
  },
  SET_CONFIGURATION({ commit }, payload) {
    commit("SET_CONFIGURATION", payload);
  },
  SET_COLORSTOPS({ commit }, payload) {
    commit("SET_COLORSTOPS", payload);
  },
  SET_FEATURE_TYPE_COUNTS({ commit }, payload) {
    commit("SET_FEATURE_TYPE_COUNTS", payload);
  },
  SET_ALL_FEATURE_TYPES({ commit }, payload) {
    commit("SET_ALL_FEATURE_TYPES", payload);
  },
  SET_VISIBLE_FEATURE_TYPES({ commit }, payload) {
    commit("SET_VISIBLE_FEATURE_TYPES", payload);
  },
  SET_ACTIVE_FEATURE_TYPE({ commit }, payload) {
    commit("SET_ACTIVE_FEATURE_TYPE", payload);
  },
  SET_MIN_TEMP({ commit }, payload) {
    commit("SET_MIN_TEMP", payload);
    eventBus.$emit("MIN_TEMP_SET", payload);
  },
  SET_MAX_TEMP({ commit }, payload) {
    commit("SET_MAX_TEMP", payload);
    eventBus.$emit("MAX_TEMP_SET", payload);
  },
  SET_TEMP_SLIDER_VALUE({ commit }, payload) {
    commit("SET_TEMP_SLIDER_VALUE", payload);
  },
  SET_VISIBLE_RASTER({ commit }, payload) {
    commit("SET_VISIBLE_RASTER", payload);
  }
};

export default new Vuex.Store({
  // strict: true,
  state,
  getters,
  mutations,
  actions
});
